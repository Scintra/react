import React from 'react';
import './App.css';

import Usuario from './componentes/Usuario'; 
import Tabela from './componentes/Tabela' ;
import Img from './componentes/Img' ;
import Formulario from './componentes/Formulario';



const App = () => {
  return (
    <div className="App">
     <h1> Hello World</h1>
     <Usuario/>
     <Tabela/>
     <Img/>
     <Formulario/>
  
    </div>
  );
}

export default App; 
// control  e ; fazem aparecer comentarios

// se eu colocar // também vai 
