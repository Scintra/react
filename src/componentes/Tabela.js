import React from 'react';
import Thead from './Thead' ;
import Tbody from './Tbody' ;
import Tfoot from './Tfoot' ; 
// nao precisa colocar o nome da pasta inteira porque já estamos nela

const Tabela = () => {
  return (
    <div className="Tabela">
    <table border ="2"> 
    <Thead/>
    <Tbody/>
    <Tfoot/>
    </table> 
    
   
  </div> );
  
}

export default Tabela;
// table é uma tag que possui fechamento, entao ela é colocada abaixo fechada. se nao tivesse, teria de ser fechada no mesmo local com uma barra antes do >






